# tempon
Time tracking for lazy people. Create and view Jira worklogs from the command line.

# Typical usage
## Log time
`tempon log "Feature A implementation"`

This will create a worklog of 8h for your default Jira issue. That is all you need most of the time!

When you need to log less hours or work for multiple issues do this:

`tempon log -i PR-12 "Test feature B" -t 4h30m`

It will log 4 hours and 30 minutes for Jira issue PR-12.

## See your worklog

To see worklog for the current week:

```
$ tempon worklog
Mon: Feature A implementation - 8h
Tue: Feature A implementation - 8h
Wed: Feature A implementation - 8h
Thu: Test feature B - 8h
Fri: Playing games and drinking beer - 8h
Total time: 40h (5.0 work days)
```

Add `-m` to see the current month:

```
$ tempon worklog -m
2023-01-02: Feature A implementation - 8h
2023-01-03: Feature A implementation - 8h
...
2023-01-20: Playing games and drinking beer - 8h
Total time: 120h 30m (15.0625 work days)
```

You can view a specified month. Multiple formats are supported, ie:

```
$ tempon worklog -m 2
$ tempon worklog -m february
$ tempon worklog -m feb
$ tempon worklog -m 2023-02
```

# Python prerequisites
```
pip3 install -r requirements.txt
```

# Setup
You will need an `Atlassian API Token` (Personal) and `Tempo Token`.

## Atlassian API Token
Create an API token from your Atlassian account:
1. Log in to https://id.atlassian.com/manage-profile/security/api-tokens.
2. Click Create API token.
3. From the dialog that appears, enter a label for your token and click Create.
4. Click Copy to clipboard, then paste the token elsewhere to save it.

## Tempo Token
Open Tempo application from Jira. Go to `Tempo > Settings`, scroll down to `Data Access` and select `API integration` then click `New Token`.

## Tempon Setup
`tempon login -s <jira subdomain> -e <your email> -a <Atlassian API Token> -t <Tempo Token>`
This is a one time setup. The tokens will be saved in a config file (plain text).
### Default issue
If you work mostly on a single issue you can set it a as a default one for logging time:
`tempon set_issue <jira issue>`

# Usage
## General
```
usage: tempon.py [-h] [--config-file CONFIG_FILE] {log,login,worklog,set_issue,clear_issue,get_issue} ...

positional arguments:
  {log,login,worklog,set_issue,clear_issue,get_issue}
    log                 log time
    login               login into your Jira account
    worklog             show worklog for the current week
    set_issue           set default issue to log time for
    clear_issue         clear default issue to log time for
    get_issue           get default issue to log time for

options:
  -h, --help                  show this help message and exit
  --config-file CONFIG_FILE   path to the config file
```

## login
```
usage: tempon.py login [-h] -e EMAIL -a API_TOKEN -t TEMPO_API_TOKEN

login into your Jira account

options:
  -s SUBDOMAIN, --subdomain SUBDOMAIN   your jira subdomain
  -e EMAIL, --email EMAIL               your email
  -a API_TOKEN, --api-token API_TOKEN   jira api token
  -t TEMPO_API_TOKEN, --tempo-api-token TEMPO_API_TOKEN tempo api token
```

## log
```
usage: tempon.py log [-h] [-i ISSUE] [-t TIME] [-d DATE] description

log time

positional arguments:
  description             description of the work done

options:
  -i ISSUE, --issue ISSUE issue to log time for
  -t TIME, --time TIME    time spent (ie. 2 (assumes hours) or '2h 30m'
  -d DATE, --date DATE    date (yyyy-mm-dd or weekday name)
```
## worklog
```
usage: tempon.py worklog [-h] [-m] [-i]

show worklog for the current week

options:
  -m, --month             show current month
  -i, --issues            show issues
  -d, --dates             show dates (instead of weekday names)
  -w WEEKS, --weeks WEEKS show WEEKS ago  
```

## set_issue
```
usage: tempon.py set_issue [-h] issue

set default issue to log time for

positional arguments:
  issue       jira issue name
```

## get_issue
```
usage: tempon.py get_issue [-h]

get default issue to log time for
```

## clear_issue
```
usage: tempon.py clear_issue [-h]

clear default issue to log time for
```
