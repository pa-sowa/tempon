#!/usr/bin/env python3
import argparse
import configparser
import os
import platform
import re
import sys
from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from pathlib import Path
from tempon.jira_client import JiraClient, UnauthorizedException
from tempon.tempo_client import TempoClient

config = configparser.ConfigParser()
config_path = None
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers(dest="subcommand")


def default_config_path():
    if platform.system() == "Windows":
        return Path(os.getenv("APPDATA")) / "tempon.conf"
    elif platform.system() == "Darwin":
        return Path.home() / "Library" / "Application Support" / "tempon.conf"
    else:
        return Path.home() / ".config" / "tempon.conf"


def save_config():
    with open(config_path, "w") as configfile:
        config.write(configfile)


def verify_config():
    try:
        email = config["default"]["email"]
        api_token = config["default"]["api_token"]
        subdomain = config["default"]["subdomain"]
        tempo_api_token = config["default"]["tempo_api_token"]
        account_id = config["default"]["account_id"]
        return True
    except KeyError:
        print("You need to login into Jira first. Run: tempon login --help")
        return False


def make_jira_client():
    email = config["default"]["email"]
    api_token = config["default"]["api_token"]
    subdomain = config["default"]["subdomain"]
    return JiraClient(email, api_token, subdomain)


def make_tempo_client():
    tempo_api_token = config["default"]["tempo_api_token"]
    account_id = config["default"]["account_id"]
    return TempoClient(tempo_api_token, account_id)


def get_issue_id(key):
    if not config.has_section("issues"):
        config.add_section("issues")
    issues_by_key = {v: k for k, v in config["issues"].items()}
    if key not in issues_by_key:
        jira = make_jira_client()
        issue = jira.get_issue(key)
        id = issue["id"]
        config["issues"][id] = key
        save_config()
        return id
    else:
        return issues_by_key[key]


def get_issue_key(id):
    id = str(id)
    if not config.has_section("issues"):
        config.add_section("issues")
    if id not in config["issues"]:
        jira = make_jira_client()
        issue = jira.get_issue(id)
        key = issue["key"]
        config["issues"][issue["id"]] = key
        save_config()
        return key
    else:
        return config["issues"][id]


def format_seconds(seconds, include_workdays=False):
    s = seconds
    workdays = s / (60 * 60 * 8)
    hours = int(s / (60 * 60))
    s -= hours * (60 * 60)
    minutes = int(s / 60)
    s -= minutes * 60
    ret = []
    if hours > 0:
        ret.append(f"{hours}h")
    if minutes > 0:
        ret.append(f"{minutes}m")
    if s > 0:
        ret.append(f"{s}s")
    if include_workdays and workdays > 0:
        ret.append(f"({workdays:.2f} work days)")
    return " ".join(ret)


def parse_time_spent(time):
    """Parses a time string which is either: '8' or '5h 35m' like and returns it as number of seconds"""
    hours = 0
    minutes = 0
    match = re.match(r"^\s*(\d+)\s*$", time)
    if match:
        hours = int(match.group(1))
    else:
        match = re.match(r"^\s*((\d+)\s*h)?\s*((\d+)\s*m)?\s*$", time)
        if match:
            hours = int(match.group(2) or "0")
            minutes = int(match.group(4) or "0")
        else:
            raise ValueError("Cannot parse time")
    seconds = hours * 60 * 60 + minutes * 60
    if seconds <= 0:
        raise ValueError("Time spent must be greater than 0")
    return seconds


def parse_date(s):
    if "-" in s:
        d = datetime.strptime(args.date, "%Y-%m-%d")
    else:
        days = {
            "monday": 0,
            "mon": 0,
            "m": 0,
            "tuesday": 1,
            "tue": 1,
            "tu": 1,
            "wednesday": 2,
            "wed": 2,
            "w": 2,
            "thursday": 3,
            "thu": 3,
            "th": 3,
            "friday": 4,
            "fri": 4,
            "f": 4,
            "saturday": 5,
            "sat": 5,
            "sa": 5,
            "sunday": 6,
            "sun": 6,
            "su": 6,
        }
        try:
            w = days[s.lower()]
        except KeyError:
            raise ValueError(f"Invalid weekday name: {s}")
        today = date.today()
        d = today - timedelta(days=(today.weekday() - w))
    return d

def parse_month(s):
    if "-" in s:
        d = datetime.strptime(s, "%Y-%m")
    elif s.isnumeric():
        m = int(s)
        d = date.today().replace(month=m)
    else:
        months = {
            "january": 1,
            "jan": 1,
            "j": 1,
            "february": 2,
            "feb": 2,
            "f": 2,
            "march": 3,
            "mar": 3,
            "april": 4,
            "apr": 4,
            "may": 5,
            "june": 6,
            "jun": 6,
            "july": 7,
            "jul": 7,
            "august": 8,
            "aug": 8,
            "september": 9,
            "sep": 9,
            "s": 9,
            "october": 10,
            "oct": 10,
            "o": 10,
            "novmeber": 11,
            "nov": 11,
            "n": 11,
            "december": 12,
            "dec": 12,
            "d": 12
        }
        try:
            m = months[s.lower()]
        except KeyError:
            raise ValueError(f"Invalid month name: {s}")
        d = date.today().replace(month=m)
    return d

def argument(*name_or_flags, **kwargs):
    """Convenience function to properly format arguments to pass to the
    subcommand decorator.
    """
    return (list(name_or_flags), kwargs)


def subcommand(args=[], parent=subparsers):
    """Decorator to define a new subcommand in a sanity-preserving way.
    The function will be stored in the ``func`` variable when the parser
    parses arguments so that it can be called directly like so::
        args = cli.parse_args()
        args.func(args)
    Usage example::
        @subcommand([argument("-d", help="Enable debug mode", action="store_true")])
        def subcommand(args):
            print(args)
    Then on the command line::
        $ python cli.py subcommand -d

    https://mike.depalatis.net/blog/simplifying-argparse.html
    """

    def decorator(func):
        parser = parent.add_parser(
            func.__name__, help=func.__doc__, description=func.__doc__
        )
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])
        parser.set_defaults(func=func)

    return decorator


@subcommand(
    [
        argument("description", help="description of the work done"),
        argument("-i", "--issue", help="issue to log time for"),
        argument("-t", "--time", help="time spent (ie. 2 (assumes hours) or '2h 30m'"),
        argument("-d", "--date", help="date (yyyy-mm-dd or weekday name)"),
    ]
)
def log(args):
    """log time"""
    if not verify_config():
        return
    if not args.issue and not "issue" in config["default"]:
        print(
            "You need to specify an issue:\ntempon log -i|--issue <jira issue>\n\nor save a default issue:\ntempon set_issue <jira issue>"
        )
        return
    issue_key = args.issue if args.issue else get_default_issue()
    issue_id = get_issue_id(issue_key)
    time_spent_seconds = 8 * 60 * 60
    if args.time:
        time_spent_seconds = parse_time_spent(args.time)
    start_date = datetime.now().date()
    if args.date:
        start_date = parse_date(args.date)
    start_date = start_date.strftime("%Y-%m-%d")
    tempo = make_tempo_client()
    tempo.create_worklog(issue_id, start_date, time_spent_seconds, args.description)


@subcommand(
    [
        argument("-s", "--subdomain", required=True, help="your jira subdomain"),
        argument("-e", "--email", required=True, help="your email"),
        argument("-a", "--api-token", required=True, help="jira api token"),
        argument("-t", "--tempo-api-token", required=True, help="tempo api token"),
    ]
)
def login(args):
    """login into your Jira account"""
    jira = JiraClient(args.email, args.api_token, args.subdomain)
    try:
        account_id = jira.get_myself_account_id()
        print("Jira authorization OK")
        config["default"]["subdomain"] = args.subdomain
        config["default"]["email"] = args.email
        config["default"]["api_token"] = args.api_token
        config["default"]["account_id"] = account_id
        try:
            tempo = TempoClient(args.tempo_api_token, account_id)
            print("Tempo authorization OK")
            config["default"]["tempo_api_token"] = args.tempo_api_token
            save_config()
        except UnauthorizedException:
            print("Error: Tempo unauthorized")
    except UnauthorizedException:
        print("Error: Jira unauthorized")


@subcommand(
    [
        argument("-m", "--month", nargs="?", const=True, help="show whole month (current if not specified)"),
        argument("-i", "--issues", action="store_true", help="show issues"),
        argument(
            "-d",
            "--dates",
            action="store_true",
            help="show dates (instead of weekday names)",
        ),
        argument("-w", "--weeks", type=int, help="show WEEKS ago"),
    ]
)
def worklog(args):
    """show worklog for the current week"""
    if not verify_config():
        return
    tempo = make_tempo_client()
    today = date.today()
    start = None
    end = None
    if args.month:
        if args.month == True:
            start = today.replace(day=1)
        else:
            start = parse_month(args.month)
        end = start + relativedelta(day=31)
    else:
        start = today - timedelta(days=today.weekday())
        end = start + timedelta(days=6)
        if args.weeks:
            start -= timedelta(weeks=args.weeks)
            end -= timedelta(weeks=args.weeks)
    logs = tempo.get_worklogs(from_date=start, to_date=end)
    total_time_spent_seconds = 0
    issues = {}
    if args.issues:
        for log in logs:
            id = log["issue"]["id"]
            issues[id] = get_issue_key(id)
    for log in logs:
        start_date = log["startDate"]
        if not args.month and not args.dates:
            start_date = datetime.strptime(start_date, "%Y-%m-%d").strftime("%a")
        issue_key = ""
        if args.issues:
            issue_key = issues[log["issue"]["id"]] + " "
        print(
            f'{start_date}: {issue_key}{log["description"]} - {format_seconds(log["timeSpentSeconds"])}'
        )
        total_time_spent_seconds += log["timeSpentSeconds"]
    print(
        f"Total time: {format_seconds(total_time_spent_seconds, include_workdays=True)}"
    )


@subcommand([argument("issue", help="jira issue name")])
def set_issue(args):
    """set default issue to log time for"""
    config["default"]["issue"] = args.issue
    save_config()


@subcommand()
def clear_issue(args):
    """clear default issue to log time for"""
    config["default"].pop("issue", None)
    save_config()


def get_default_issue():
    if "issue" in config["default"]:
        return config["default"]["issue"]
    else:
        return None


@subcommand()
def get_issue(args):
    """get default issue to log time for"""
    print(get_default_issue() or "No default issue")


if __name__ == "__main__":
    parser.add_argument("--config-file", help="path to the config file")
    args = parser.parse_args()

    config_path = (
        str(default_config_path()) if args.config_file is None else args.config_file
    )
    config.read(config_path)
    default_config = config["default"] if config.has_section("default") else None
    if not config.has_section("default"):
        config.add_section("default")

    if not args.subcommand:
        parser.print_help()
    else:
        args.func(args)
