import requests
import json


class UnauthorizedException(Exception):
    "Raised when the input value is less than 18"
    pass


class NotFoundException(Exception):
    "Object does not exist or you do not have permission to see it"
    pass


class JiraException(Exception):
    "Unspecifed Jira error"


class RestClient:
    base_url = None
    headers = None
    auth = None

    def __init__(self, base_url, headers=None, auth=None):
        self.base_url = base_url
        self.headers = headers
        self.auth = auth

    def make_request(self, path, method="GET", data=None):
        url = f"{self.base_url}/{path}"
        # print(f"Request: {url}")
        headers = self.headers or {}
        if method == "POST":
            headers["Content-Type"] = "application/json"
        response = requests.request(
            method, url, headers=self.headers, auth=self.auth, data=data
        )
        # print(response.status_code)
        # print(response.text)
        if response.status_code == 401:
            raise UnauthorizedException()
        elif response.status_code == 404:
            raise NotFoundException()
        elif response.status_code / 100 != 2:
            raise ServerException()
        j = json.loads(response.text)
        # print(json.dumps(j, sort_keys=True, indent=4, separators=(",", ": ")))
        return j
