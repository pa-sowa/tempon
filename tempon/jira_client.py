#!/usr/bin/env python3
from requests.auth import HTTPBasicAuth
from .rest_client import *


class JiraClient(RestClient):
    email = None
    api_token = None

    def __init__(self, email, api_token, subdomain):
        auth = HTTPBasicAuth(email, api_token)
        super().__init__(f"https://{subdomain}.atlassian.net", auth=auth)
        self.email = email
        self.api_token = api_token

    def get_myself(self):
        return self.make_request("/rest/api/3/myself")

    def get_myself_account_id(self):
        return self.get_myself()["accountId"]

    def get_issue(self, issue_id_or_key):
        return self.make_request(f"rest/api/3/issue/{issue_id_or_key}")

    def get_issue_worklogs(self, issue_id_or_key):
        return self.make_request(f"rest/api/3/issue/{issue_id_or_key}/worklog")

    def add_issue_worklog(self, issue_id_or_key, timeSpentSeconds):
        return self.make_request(
            f"rest/api/3/issue/{issue_id_or_key}/worklog", method="POST"
        )
