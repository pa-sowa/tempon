#!/usr/bin/env python3
import json
from .rest_client import *


class TempoClient(RestClient):
    api_token = None
    account_id = None

    def __init__(self, tempo_api_token, account_id):
        headers = {"Authorization": f"Bearer {tempo_api_token}"}
        super().__init__("https://api.tempo.io", headers=headers)
        self.tempo_api_token = tempo_api_token
        self.account_id = account_id

    def get_accounts(self):
        path = "4/accounts"
        return self.make_request(path)

    def get_worklogs(self, issue_id=None, from_date=None, to_date=None):
        path = "4/worklogs"
        query_args = []
        if issue_id:
            query_args.append(f"issueId={issue_id}")
        if from_date:
            query_args.append(f"from={from_date.strftime('%Y-%m-%d')}")
        if to_date:
            query_args.append(f"to={to_date.strftime('%Y-%m-%d')}")
        if query_args:
            path += "?" + "&".join(query_args)
        return self.make_request(path)["results"]

    def create_worklog(self, issue_id, start_date, time_spent_seconds, description):
        payload = {
            "authorAccountId": self.account_id,
            "issueId": issue_id,
            "startDate": start_date,
            "timeSpentSeconds": time_spent_seconds,
            "description": description,
        }
        return self.make_request("4/worklogs", method="POST", data=json.dumps(payload))
